import { square, reverse, date } from "./service.js";
import Joi from "joi";

const getSquare = async (req, res) => {
  try {
    const schema = Joi.number().required();
    const { error, value } = schema.validate(Number(req.body));
    if (error) throw error;
    const result = await square(value);
    return res.status(201).json(result);
  } catch (error) {
    return res
      .status(500)
      .json({ error: "Internal server error", status: 500 });
  }
};

const getReverse = async (req, res) => {
  try {
    const schema = Joi.string().required();
    const { error, value } = schema.validate(req.body);
    if (error) throw error;
    const result = await reverse(value);
    return res.status(201).send(result);
  } catch (error) {
    return res.status(500).send("Internal server error");
  }
};

const getDate = async (req, res) => {
  try {
    const schema = Joi.object({
      year: Joi.number().required(),
      month: Joi.number().required(),
      day: Joi.number().required(),
    });
    const { error, value } = schema.validate(req.params);
    if (error) throw error;
    const result = await date(value.year, value.month, value.day);
    return res.status(201).json(result);
  } catch (error) {
    return res.status(500).json("Internal server error");
  }
};

export { getSquare, getReverse, getDate };
