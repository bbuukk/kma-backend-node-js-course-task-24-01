import express from "express";

import { getSquare, getReverse, getDate } from "./controller.js";

const router = express.Router();

router.get("/date/:year/:month/:day", getDate);
router.post("/square", getSquare);
router.post("/reverse", getReverse);

export { router };
