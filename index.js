import dotenv from "dotenv";
dotenv.config();
import express from "express";

import { router } from "./routes.js";

const app = express();

app.use(express.text());

app.use("/", router);

const port = process.env.PORT || 56201;
app.listen(port, () => {
  console.log(`listening on port ${port}`);
});
