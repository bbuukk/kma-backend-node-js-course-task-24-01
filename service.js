import moment from "moment";

const square = async (number) => ({ number, square: number ** 2 });

const reverse = async (text) => text.split("").reverse().join("");

const date = async (year, month, day) => {
  const date = moment(`${year}-${month}-${day}`, "YYYY-MM-DD");
  const today = moment().startOf("day");
  if (!date.isValid()) {
    return res.status(400).json({ error: "Invalid date" });
  }
  const weekDay = date.format("dddd");
  const isLeapYear = date.isLeapYear();
  const difference = Math.abs(today.diff(date, "days"));
  return {
    weekDay,
    isLeapYear,
    difference,
  };
};

export { square, reverse, date };
